<?php

declare(strict_types=1);

namespace App\Task2;

class EmojiGenerator
{
    private const EMOJI = ['🚀', '🚃', '🚄', '🚅', '🚇'];

    public function generate(): \Generator
    {
        foreach (self::EMOJI as $key => $value){
            yield $key => $value;
        }
    }
}
