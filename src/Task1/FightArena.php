<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $fighters;

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        //sort array from low to high value of fighter`s attack  using user sorting function
        usort($this->fighters, function(Fighter $fighter1, Fighter $fighter2){return ($fighter1->getAttack() <=> $fighter2->getAttack());});
        //return last element of array
        return array_pop($this->fighters);
    }

    public function mostHealthy(): Fighter
    {
        //sort array from low to high value of fighter`s health using user sorting function
        usort($this->fighters, function(Fighter $fighter1, Fighter $fighter2){return ($fighter1->getHealth() <=> $fighter2->getHealth());});
        //return last element of array
        return array_pop($this->fighters);
    }

    public function all(): array
    {
        return $this->fighters;
    }
}
