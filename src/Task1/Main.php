<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task1\Fighter;

$ryu = new Fighter(1, "Ryu", 90, 96, "https://bit.ly/2E5Pouh");
$chunLi = new Fighter(2,"Chun-Li", 85, 89, "https://bit.ly/2Vie3lf");
$kenMasters = new Fighter(3, "Ken Masters", 100, 90, "https://bit.ly/2VZ2tQd");

var_dump($ryu);
var_dump($chunLi);
var_dump($kenMasters);