<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $fighters = $arena->all();
        $presentation = "<div class=\"ui three stackable cards centered\">";
        foreach ($fighters as $fighter){
            $presentation .= "<div class=\"card\">
                                  <div class=\"image\">
                                    <img src=\"{$fighter->getImage()}\">
                                  </div>
                                  <div class=\"content\">
                                    <a class=\"header\">{$fighter->getName()}: {$fighter->getHealth()}, {$fighter->getAttack()}</a>
                                  </div>
                             </div>";

        }

        $presentation .= "</div>";

        return $presentation;
    }
}
